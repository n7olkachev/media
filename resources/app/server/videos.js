const fs = require('fs')

const readFile = file =>
    new Promise((res, rej) =>
        fs.readFile(file, 'utf8', (err, data) =>
            err ? rej(err) : res(data)
        )
    )

class Videos {
    async load () {
        const json = await readFile(`${__dirname}/../data.json`)
        this.data = JSON.parse(json)
    }

    find (id) {
        for (let category of Object.values(this.data)) {
            for (let subCategory of Object.values(category)) {
                for (let videos of Object.values(subCategory)) {
                    const video = videos.find(video => video.id == id)
                    if (video) {
                        return video
                    }
                }
            }
        }
    }
}

module.exports = new Videos()