const fs = require('fs')

module.exports = class DataBase {
    constructor () {
        this.data = {}
    }

    load () {
        return new Promise((res, rej) => {
            fs.readFile(`${__dirname}/db.txt`, 'utf8', (err, json) => {
                if (json) {
                    this.data = JSON.parse(json)
                }

                res()
            })
        })
    }

    save () {
        return new Promise((res, rej) => {
            fs.writeFile(`${__dirname}/db.txt`, JSON.stringify(this.data, null, 2), (err, result) => err ? rej(err) : res())
        })
    }
}