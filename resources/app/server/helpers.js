const fs = require('fs')
const path = require('path')

exports.redirect = (res, url) => {
    res.writeHead(302, { 'Location': url })
    res.end()
}

exports.view = (res, view) => {
    res.writeHead(200, { 'Content-Type': 'text/html' })
    fs.createReadStream(`${__dirname}/views/${view}.html`).pipe(res)
}

function encodeRFC5987ValueChars (str) {
    return encodeURIComponent(str)
        .replace(/['()]/g, escape)
        .replace(/\*/g, '%2A')
        .replace(/%(?:7C|60|5E)/g, unescape);
}

const download = exports.download = (res, filePath, disposition = false) => {
    let stat = null

    try {
        stat = fs.statSync(filePath);
    } catch (e) {
        res.writeHead(404)
        res.end()
        return
    }
    const extname = path.extname(filePath)
    let mime = 'application/octet-stream'

    switch (extname) {
        case '.mov':
            mime = 'video/quicktime'
            break
        case '.mp4':
            mime = 'video/mp4'
            break
        case '.css':
            mime = 'text/css'
            break
        case '.png':
            mime = 'image/png'
            break
        case '.jpg':
            mime = 'image/jpg'
            break
        case '.woff2':
            mime = 'font/woff2'
            break
    }

    const headers = {
        'Content-Type': mime,
        'Content-Length': stat.size,
    }

    if (disposition) {
        headers['Content-Disposition'] = `attachment; filename*=UTF-8''${encodeRFC5987ValueChars(path.basename(filePath))}`
    }

    res.writeHead(200, headers);

    fs.createReadStream(filePath).pipe(res);
}

exports.downloadVideo = (res, video) => {
    download(res, `${__dirname}/../videos/${video}`, true)
}