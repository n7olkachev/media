const { view, downloadVideo, download, redirect } = require('./helpers')
const config = require('../config')
const videos = require('./videos')
const { parse } = require('url')
const execa = require('execa')
const http = require('http')
const DataBase = require('./db')
const db = new DataBase()

const app = async (req, res) => {
    const { pathname, query } = parse(req.url, true)

    if (pathname == '/videos') {
        view(res, 'videos')
    } else if (pathname == '/download') {
        const { id } = query
        const video = videos.find(id)
        if (video) {
            db.data[video.id] = (db.data[video.id] || 0) + 1
            await db.save()
            downloadVideo(res, video.file_small)
        } else {
            redirect(res, 'http://192.168.12.1/videos')
        }
    } else if (pathname.startsWith('/static')) {
        const [_, ...path] = pathname.split('/')
        download(res, `${__dirname}/${path.join('/')}`)
    } else {
        redirect(res, 'http://192.168.12.1/videos')
    }
}

module.exports = async () => {
    execa('create_ap', `${config.interface} ${config.ethernet_interface} ${config.ssid} ${config.password}`.split(' '))
    execa('sysctl', `net.ipv4.ip_forward=1`.split(' '))
    execa('sysctl', `-w net.ipv4.conf.all.route_localnet=1`.split(' '))
    execa('iptables', `-t nat -I PREROUTING -p tcp --dport 80 -j DNAT --to 127.0.0.1:80`.split(' '))
    await videos.load()
    await db.load()
    http.createServer(app).listen(80)
    http.createServer(app).listen(443)
    console.log('Server is running')
}
