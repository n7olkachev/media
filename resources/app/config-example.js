module.exports = {
    width: 800,
    height: 600,
    skipTaskbar: true,
    autoHideMenuBar: true,
    overlayScrollbars: true,
    fullscreen: true,
    ssid: 'ExpoBelarus_1',
    password: '12345678',
    interface: 'wlp2s0',
    ethernet_interface: 'enp1s0',
}
