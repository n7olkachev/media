const nodemailer = global.require('nodemailer')
const transporter = nodemailer.createTransport({
    host: 'smtp.gmail.com',
    port: 465,
    auth: {
        user: '2017expobelarus@gmail.com',
        pass: 'expobelarus2017',
    },
    tls: {
        rejectUnauthorized: false
    }
})

export default (to, text) => {
    const mailOptions = {
        from: 'expobelarus2017 <2017expobelarus@gmail.com',
        to,
        subject: 'Ссылка на видео',
        text
    }

    return transporter.sendMail(mailOptions)
}