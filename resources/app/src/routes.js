import { Switch, Route } from 'react-router'
import React from 'react'

import HomePage from './containers/home-page'
import CategoriesPage from './containers/categories-page'
import VideosPage from './containers/videos-page'

export default () => (
    <Switch>
      <Route path="/" component={HomePage} exact />
      <Route path="/:language" component={CategoriesPage} exact />
      <Route path="/:language/:path" component={VideosPage} />
    </Switch>
);
