import React from 'react'

export default ({ categories, setCategory }) => (
    <div className="categories">
        {Object.keys(categories).map(category =>
            <div className="category">
                <h2 className="category__header">{category}</h2>
                <div className="category__sub-categories">
                    {Object.keys(categories[category]).map(subCategory =>
                        <div className="category__sub-category" onClick={() => setCategory(`${category}$$$${subCategory}`)}>
                            {subCategory}
                        </div>
                    )}
                </div>
            </div>
        )}
    </div>
)