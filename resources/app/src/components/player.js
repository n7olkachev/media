import DownloadVideo from './download-video'
import React, { Component } from 'react'
import SendEmail from './send-email'

export default class Player extends Component {
    state = {
        ended: false,
        sendEmail: false,
        downloadVideo: false,
        playing: true,
    }

    video = null

    activityInterval = null

    componentDidMount () {
        this.activityInterval = setInterval(() => this.state.playing && this.props.ping(), 1000)
    }

    componentWillUnmount () {
        clearInterval(this.activityInterval)
    }

    setVideo = video => {
        if (!video) {
            this.video.removeEventListener('ended', this.onEnd)
            this.video = video
        } else {
            this.video = video
            video.addEventListener('ended', this.onEnd)
        }
    }

    onEnd = () => {
        this.setState({
            ended: true,
            playing: false,
        })
    }

    play = () => {
        this.setState({
            ended: false,
            playing: true,
        })
        this.video.play()
    }

    pause = () => {
        this.setState({
            playing: false,
        })
        this.video.pause()
    }

    next = () => {
        if (this.state.ended) {
            this.props.onClose()
        } else {
            this.video.currentTime = this.video.duration
        }
    }

    sendEmail = () => {
        this.setState({ sendEmail : true })
    }

    togglePlaying = () => {
        if (this.state.playing) {
            this.pause()
        } else {
            this.play()
        }
    }

    render () {
        const { video, onClose, trans, goToMenu } = this.props

        return (
            <div className="player">
                <video onClick={this.togglePlaying} className="player__video" src={`./videos/${video.file}`} ref={this.setVideo} autoPlay style={{ opacity : this.state.playing ? 1 : 0.2 }} />
                {!this.state.playing &&
                    <div className="player__close" onClick={onClose}>
                        <span className="fa fa-close" />
                    </div>
                }
                {this.state.playing &&
                    <div onClick={this.togglePlaying} className="player__option">
                        <span className="fa fa-bars" />
                    </div>
                }
                {!this.state.playing &&
                    <div className="player__controls">
                        <div className="player__control-buttons">
                            <div className="player__control player__control--text" onClick={this.sendEmail}>
                                {trans('sendEmail')}
                            </div>
                            <div className="player__control player__control--text player__control--text--green" onClick={() => this.setState({ downloadVideo: true })}>
                                {trans('download')}
                            </div>
                        </div>
                        <div className="player__control-buttons">
                            <div className="fa fa-play-circle player__control" onClick={this.play} />
                        </div>
                        <div className="player__control-buttons">
                            <div className="player__control player__control--text" onClick={this.next}>
                                {this.state.ended ? trans('close') : trans('next')}
                            </div>
                            <div className="player__control player__control--text" onClick={goToMenu}>
                                {trans('goToMenu')}
                            </div>
                        </div>
                    </div>
                }
                {this.state.sendEmail && <SendEmail trans={trans} video={video} onClose={() => this.setState({ sendEmail : false })} />}
                {this.state.downloadVideo && <DownloadVideo trans={trans} video={video} onClose={() => this.setState({ downloadVideo : false })} />}
            </div>
        )
    }
}