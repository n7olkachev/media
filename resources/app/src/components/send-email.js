import KeyboardedInput from 'react-touch-screen-keyboard';
import React, { Component } from 'react'
import sendEmail from '../services/mail'

const fs = global.require('fs')
const writeFile = (file, data) => new Promise((res, rej) =>
    fs.appendFile(file, data, (err, result) => err ? rej(err) : res(result)))

export default class SendEmail extends Component {
    state = {
        step: 1,
        email: '',
    }

    sendEmail = () => {
        if (!this.isValidEmail()) return

        writeFile('./email.txt', `${this.state.email}:${this.props.video.link}\n`)
        sendEmail(this.state.email, this.props.video.link)
        this.setState({ step : 2 })
    }

    isValidEmail = () => /\S{2,}@\S{2,}\.\S{2,}/.test(this.state.email)

    renderFirstStep = () => {
        const { trans } = this.props

        return (
            <div className="send-email__content">
                <div className="player__close player__close--popup" onClick={this.props.onClose}>
                    <span className="fa fa-close" />
                </div>
                <div className="send-email__header">{trans('yourEmail')}:</div>
                <KeyboardedInput
                    enabled
                    onChange={email => this.setState({ email })}
                    value={this.state.email}
                    className="send-email__input"
                    defaultKeyboard="us"
                />
                <div className={`send-email__button ${this.isValidEmail() ? 'send-email__button--active' : ''}`} onClick={this.sendEmail}>
                    {trans('send')}
                </div>
                {this.state.input &&
                    <Keyboard
                        inputNode={this.state.input}
                        leftButtons={[]}
                        rightButtons={[]}
                        layouts={[LatinLayout]}
                    />
                }
            </div>
        )
    }

    renderSecondStop = () => {
        const { trans } = this.props

        return (
            <div className="send-email__content">
                <div className="player__close player__close--popup" onClick={this.props.onClose}>
                    <span className="fa fa-close" />
                </div>
                <div className="send-email__header">{trans('messageSent')}</div>
            </div>
        )
    }

    render () {
        return (
            <div className="send-email">
                {this.state.step == 1 ? this.renderFirstStep() : this.renderSecondStop()}
            </div>
        )
    }
}