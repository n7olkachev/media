import React, { Component } from 'react'

export default class DownloadVideo extends Component {
    render () {
        const { trans, video } = this.props

        return (
            <div className="download-video">
                <div className="send-email__content">
                    <div className="player__close player__close--popup" onClick={this.props.onClose}>
                        <span className="fa fa-close" />
                    </div>
                    <div className="send-email__header">{trans('downloadVideoMessage')(video.id)}</div>
                </div>
            </div>
        )
    }
}