import React, { Component } from 'react'

const clamp = (x, min, max) => Math.max(min, Math.min(max, x))

export default class Videos extends Component {
    state = {
        page: 0,
    }

    videos = () => this.props.videos.slice(this.state.page * 6, (this.state.page + 1) * 6)

    minPage = () => 0

    maxPage = () => Math.ceil(this.props.videos.length / 6) - 1

    upActive = () => this.state.page > this.minPage()

    downActive = () => this.state.page < this.maxPage()

    changePage = diff => () => this.setState({
        page : clamp(this.state.page + diff, this.minPage(), this.maxPage())
    })

    render () {
        const { onSelect } = this.props
        const videos = this.videos()

        return (
            <div className="videos-wrapper">
                <div className="videos">
                    {videos.map(video =>
                        <div className="video" onClick={() => onSelect(video)}>
                            <div className="fa fa-play-circle video__play"></div>
                            <div className="video__title">{video.name}</div>
                        </div>
                    )}
                </div>
                {this.props.videos.length > 6 &&
                    <div className="videos__scroll">
                        <div
                            onClick={this.changePage(-1)}
                            className={`${this.upActive() ? '' : 'videos__scroll-button--inactive' } videos__scroll-button fa fa-arrow-circle-up`}
                        />
                        <div
                            onClick={this.changePage(+1)}
                            className={`${this.downActive() ? '' : 'videos__scroll-button--inactive' } videos__scroll-button fa fa-arrow-circle-down`}
                        />
                    </div>
                }

            </div>
        )
    }
}