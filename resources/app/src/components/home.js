import React, { Component } from 'react'

export default class Home extends Component {
    state = {
        loaded: false,
    }

    componentDidMount () {
        this.load()
    }

    load = async () => {
        const { loadData } = this.props

        await loadData()

        this.setState({ loaded : true })
    }

    render () {
        const { setLanguage } = this.props
        const { loaded } = this.state

        if (!loaded) return false
        return (
            <div>
                <div className="home-info home-info--left">
                    <div className="home-info__box">
                        <div className="home-info__title">ДОБРО ПОЖАЛОВАТЬ</div>
                        <div className="home-info__sub-title">в мир белорусских инноваций!</div>
                    </div>
                    <div className="home-info__text">НАШИ ПРОЕКТЫ БОЛЬШЕ, <br/> ЧЕМ БИЗНЕС</div>
                </div>
                <div className="home-container">
                    <div className="language">
                        <div className="language__box language__box--left">
                            <div className="language__text language__text--left">ВЫБЕРИТЕ ЯЗЫК</div>
                            <div className="language__btn language__btn--ru" onClick={() => setLanguage('ru')}></div>
                        </div>
                        <div className="language__box">
                            <div className="language__text  language__text--right">CHOOSE LANGUAGE</div>
                            <div className="language__btn language__btn--en" onClick={() => setLanguage('en')}></div>
                        </div>
                    </div>
                </div>

                <div className="home-info home-info--right">
                    <div className="home-info__box">
                        <div className="home-info__title">WELCOME</div>
                        <div className="home-info__sub-title">to the world of Belarusian innovations!</div>
                    </div>
                    <div className="home-info__text">OUR PROJECTS ARE GREATER <br/> THAN JUST BUSINESS</div>
                </div>
            </div>
        )

    }
}