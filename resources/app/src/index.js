import { configureStore } from './store/configureStore'
import { history } from './store/enhancer'
import Root from './containers/root'
import { render } from 'react-dom'
import React from 'react'

const store = configureStore()

const { webFrame } = global.require('electron')
webFrame.setVisualZoomLevelLimits(1, 1)

render(
    <Root store={store} history={history} />,
    document.getElementById('root')
)