import { handleActions, createAction } from "redux-actions"
const config = global.require('./config')
import React from 'react'

const messages = {
    ru: {
        back: 'Назад',
        sendEmail: 'На Email',
        download: 'Скачать',
        close: 'Закрыть',
        next: 'Контакты',
        goToMenu: 'Главное меню',
        yourEmail: 'Введите Вашу электронную почту',
        send: 'Отправить',
        messageSent: 'Спасибо, письмо отправлено!',
        downloadVideoMessage: id =>
            <span>
                Подключитесь к WiFi: <strong>«ExpoBelarus»</strong> <br/>
                Введите пароль: <strong>{config.password}</strong> <br />
                Номер инновационной разработки: <strong className="download__text-big">{id}</strong>
            </span>,
    },
    en: {
        back: 'Back',
        sendEmail: 'Email',
        download: 'Download',
        close: 'Close',
        next: 'Contacts',
        goToMenu: 'Main menu',
        yourEmail: 'If you are interested in the development, we can send this video to your e-mail right now',
        send: 'Send',
        messageSent: 'Thank you, the letter has been sent. Check your e-mail.',
        downloadVideoMessage: id =>
            <span>
                Connect to WiFi: <strong>«ExpoBelarus»</strong> <br/>
                Type in the password: <strong>{config.password}</strong> <br />
                Innovative product: <strong className="download__text-big">{id}</strong>
            </span>,
    }
}

export const trans = state => message => messages[state.lang][message]

export const setLanguage = createAction('SET_LANGUAGE', lang => lang)

export default handleActions({
    [setLanguage]: (state, action) => action.payload
}, null)