import { createStore, combineReducers } from 'redux'
import enhancer from './enhancer'
import data from './data'
import lang from './lang'

const rootReducer = combineReducers({
    data,
    lang
})

export function configureStore(initialState) {
    return createStore(rootReducer, initialState, enhancer)
}