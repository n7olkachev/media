import { handleActions, createAction } from "redux-actions"
const fs = global.require('fs')

const readFile = file => new Promise((res, rej) => {
    fs.readFile(file, 'utf8', (err, result) => err ? rej(err) : res(result))
})

export const loadData = createAction('LOAD_DATA', async () => {
    return JSON.parse(await readFile(window.APP_ROOT + '/data.json'))
})

export default handleActions({
    [loadData]: (state, action) => action.payload
}, null)