import { routerMiddleware, push } from 'react-router-redux'
import { createMemoryHistory } from 'history'
import { applyMiddleware } from 'redux'
import promise from 'redux-promise'

const logger = store => next => action => {
    console.group(action.type)
    console.info('dispatching', action)
    let result = next(action)
    console.log('next state', store.getState())
    console.groupEnd(action.type)
    return result
}

const activity = store => {
    const goToHome = () => {
        store.dispatch(push('/'))
        timeout = createActivityTimer()
    }
    const createActivityTimer = () => setTimeout(goToHome, 120 * 1000)
    let timeout = createActivityTimer()

    return next => action => {
        if (action.type == 'ACTIVITY') {
            clearTimeout(timeout)
            timeout = createActivityTimer()
        }

        return next(action)
    }
}

export const history = createMemoryHistory()
const router = routerMiddleware(history)

export default applyMiddleware(
    activity,
    logger,
    promise,
    router,
)