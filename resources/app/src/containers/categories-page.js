import Categories from '../components/categories'
import { push } from 'react-router-redux'
import BaseLayout from './base-layout'
import { connect } from 'react-redux'
import React from 'react'

const CategoriesPage = ({ categories, setCategory }) => (
    <BaseLayout style={{ backgroundImage: `url(./images/home.png)`}}>
        <Categories
            categories={categories}
            setCategory={setCategory}
        />
    </BaseLayout>
)

const mapDispatchToProps = (dispatch, { match }) => ({
    setCategory: category => dispatch(push(`${match.url}/${category}`))
})

const mapStateToProps = (state, { match }) => ({
    categories: state.data[match.params.language]
})

export default connect(mapStateToProps, mapDispatchToProps)(CategoriesPage)
