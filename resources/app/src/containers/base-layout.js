import { goBack } from 'react-router-redux'
import { connect } from 'react-redux'
import { trans } from '../store/lang'
import React from 'react'

const BaseLayout = ({ goBack, children, style, trans, onActivity }) => (
    <div className="base-container" style={style} onClick={onActivity}>
        <div className="base-container__header">
            <div className="base-container__back-button" onClick={goBack}>
                {trans('back')}
            </div>
        </div>
        <div className="base-container__content">
            {children}
        </div>
    </div>
)

const mapDispatchToProps = dispatch => ({
    goBack: () => dispatch(goBack()),
    onActivity: () => {
        dispatch({ type : 'ACTIVITY' })
    }
})

const mapStateToProps = state => ({
    trans: trans(state),
})

export default connect(mapStateToProps, mapDispatchToProps)(BaseLayout)
