import Player from '../components/player'
import Videos from '../components/videos'
import React, { Component } from 'react'
import { go } from 'react-router-redux'
import BaseLayout from './base-layout'
import { connect } from 'react-redux'
import { trans } from '../store/lang'

class VideosPage extends Component {
    state = {
        selectedVideo: null,
    }

    onSelect = video => {
        this.setState({ selectedVideo : video })
    }

    closePlayer = () => {
        this.setState({ selectedVideo : null })
    }

    render () {
        const { videos, trans, goToMenu, ping } = this.props
        const { selectedVideo } = this.state

        return (
            <BaseLayout style={{ backgroundImage: `url(./images/home.png)`}}>
                <Videos videos={videos} onSelect={this.onSelect} />
                {selectedVideo &&
                    <Player video={selectedVideo} onClose={this.closePlayer} trans={trans} goToMenu={goToMenu} ping={ping} />
                }
            </BaseLayout>
        )
    }
}

const mapStateToProps = (state, { match: { params } }) => {
    const [ category, subCategory ] = params.path.split('$$$')

    return {
        videos: state.data[params.language][category][subCategory],
        trans: trans(state),
        lang: state.lang,
    }
}

const mapDispatchToProps = dispatch => ({
    goToMenu: () => dispatch(go(-1)),
    ping: () => dispatch({ type : 'ACTIVITY' })
})

export default connect(mapStateToProps, mapDispatchToProps)(VideosPage)
