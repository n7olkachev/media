import { setLanguage } from '../store/lang'
import { push } from 'react-router-redux'
import { loadData } from '../store/data'
import Home from '../components/home'
import { connect } from 'react-redux'

const mapDispatchToProps = dispatch => ({
    setLanguage: language => {
        dispatch(setLanguage(language))
        dispatch(push(`/${language}`))
    },
    loadData: () => dispatch(loadData()),
})

export default connect(null, mapDispatchToProps)(Home)
