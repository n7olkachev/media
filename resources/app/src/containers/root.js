import { ConnectedRouter } from 'react-router-redux'
import { Provider } from 'react-redux'
import Routes from '../routes'
import React from 'react'

export default ({ store, history }) => (
    <Provider store={store}>
      <ConnectedRouter history={history}>
        <Routes />
      </ConnectedRouter>
    </Provider>
)