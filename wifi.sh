#!/bin/bash
sysctl -w net.ipv4.ip_forward=1
sysctl -w net.ipv4.conf.all.route_localnet=1
iptables -t nat -I PREROUTING -p tcp --dport 80 -j DNAT --to 127.0.0.1:1337
iptables -t nat -I PREROUTING -p tcp --dport 443 -j DNAT --to 127.0.0.1:1338
