#!/bin/bash
sudo apt-get update
sudo apt-get install hostapd dnsmasq curl openssh-server
curl -sL https://deb.nodesource.com/setup_8.x | sudo -E bash -
cd resources/app
npm i --production
sudo apt-get install -y nodejs
cd /tmp
git clone https://github.com/oblique/create_ap
cd create_ap
sudo make install
echo "$USER ALL=(ALL) NOPASSWD:ALL" | sudo tee -a /etc/sudoers

# nmcli device wifi hotspot con-name my-hotspot ssid WYFY band bg password 12345678
# git clone git@46.101.126.52:Nikita/mediacube-infobox-public.git
